﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using CombatRotation.RotationFramework;
using robotManager;
using robotManager.Helpful;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using wManager.Wow.Bot.States;
using wManager.Events;
using wManager;
using wManager.Wow.Enums;
using Uhtred.Specials;

public class Main : ICustomClass
{
    public string ProjectName => "Priest";
    public float Range => NeedsRangePull() ? 45f : 5f;


    private bool _isLaunched;
    private static WoWLocalPlayer Me = ObjectManager.Me;


    private static RotationSpell DesperatePrayer    = new RotationSpell("Desperate Prayer");
    private static RotationSpell DivineStar         = new RotationSpell("Divine Star");
    private static RotationSpell Evangelism         = new RotationSpell("Evangelism");
    private static RotationSpell Halo               = new RotationSpell("Halo");
    private static RotationSpell HolyNova           = new RotationSpell("Holy Nova");
    private static RotationSpell Levitate           = new RotationSpell("Levitate");
    private static RotationSpell PainSuppression    = new RotationSpell("Pain Suppression");
    private static RotationSpell Penance            = new RotationSpell("Penance");
    private static RotationSpell PowerWordBarrier   = new RotationSpell("Power Word: Barrier");
    private static RotationSpell PowerWordFortitude = new RotationSpell("Power Word: Fortitude");
    private static RotationSpell PowerWordRadiance  = new RotationSpell("Power Word: Radiance");
    private static RotationSpell PowerWordSolace    = new RotationSpell("Power Word: Solace");
    private static RotationSpell PowerWordShield    = new RotationSpell("Power Word: Shield");
    private static RotationSpell PurgetheWicked     = new RotationSpell("Purge the Wicked");
    private static RotationSpell Rapture            = new RotationSpell("Rapture");
    private static RotationSpell Schism             = new RotationSpell("Schism");
    private static RotationSpell Shadowfiend        = new RotationSpell("Shadowfiend");
    private static RotationSpell ShadowMend         = new RotationSpell("Shadow Mend");
    private static RotationSpell ShadowWordPain     = new RotationSpell("Shadow Word: Pain");
    private static RotationSpell Smite              = new RotationSpell("Smite");

    private List<RotationStep> RotationSpells = new List<RotationStep>{
        new RotationStep(ShadowWordPain, 0.5f, (a, t) => !t.HasBuff(ShadowWordPain.Spell.NameInGame), RotationCombatUtil.BotTarget),
        new RotationStep(Smite, 0.5f, (a, t) => !t.HasBuff(ShadowWordPain.Spell.NameInGame), RotationCombatUtil.BotTarget),
//        new RotationStep(Halo, 0.9f, (a, t) => t.HealthPercent < 70 && t.ManaPercentage < 35 && RotationFramework.Target.HealthPercent > 80, RotationCombatUtil.FindMe),
//        new RotationStep(HolyNova, 0.9f, (a, t) => t.ManaPercentage < 10, RotationCombatUtil.FindMe),
//        new RotationStep(Schism, 0.95f, (a, t) => !Me.InCombatFlagOnly && NeedsRangePull() && t.HealthPercent == 100, RotationCombatUtil.BotTarget),
//        new RotationStep(Smite, 1.0f, (a, t) => t.HealthPercent < 35, RotationCombatUtil.FindMe),

    };

    public void Initialize()
    {
        PriestDisciplineSettings.Load();
        wManagerSetting.CurrentSetting.UseLuaToMove = true;
        RotationFramework.Initialize(PriestDisciplineSettings.CurrentSetting.SlowRotation, PriestDisciplineSettings.CurrentSetting.FrameLock);
        _isLaunched = true;
        RotationSpells.Sort((a, b) => a.Priority.CompareTo(b.Priority));
        Rotation();
        Uhtred.Specials.Logger logger = new Uhtred.Specials.Logger("Priest");
        logger.Log("Initialized");

    }

    public void Rotation()
    {
        while (_isLaunched)
        {
            try
            {
                if (Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause && !Me.IsDead)
                {

                    UseBuffs();
                    if(Fight.InFight)
                    {
                        RotationFramework.RunRotation(RotationSpells);
                    }
                }
            }
            catch(Exception e)
            {
                robotManager.Helpful.Logging.WriteError("ExampleClass ERROR:" + e);
            }
            
            Thread.Sleep(Usefuls.LatencyReal);
        }
    }


    private void UseBuffs()
    {
        if (Me.IsFallingFar)
            RotationCombatUtil.CastBuff(Levitate);
        if (Me.IsMounted || Me.InCombatFlagOnly || Fight.InFight)
            return;
        if (Me.HealthPercent < 60)
            RotationCombatUtil.CastSpell(ShadowMend, Me);
        if (!Me.HasBuff(PowerWordFortitude.Spell.NameInGame))
            RotationCombatUtil.CastSpell(PowerWordFortitude, Me);

    }


    
    private static bool NeedsRangePull()
    {
        if (Fight.CombatStartSince > 8000)
        {
            return false;
        }
        
        return RotationFramework.Units.FirstOrDefault(o =>
                   o.IsAlive && o.Reaction == Reaction.Hostile &&
                   o.Guid != RotationFramework.Target.Guid &&
                   o.Position.DistanceTo(RotationFramework.Target.Position) <= PriestDisciplineSettings.CurrentSetting.MaxRange) != null;
    }

    public void Dispose()
    {
        _isLaunched = false;
        RotationFramework.Dispose();
    }

    public void ShowConfiguration()
    {
        PriestDisciplineSettings.Load();
        PriestDisciplineSettings.CurrentSetting.ToForm();
        PriestDisciplineSettings.CurrentSetting.Save();
    }

}

/*
 * SETTINGS
*/

[Serializable]
public class PriestDisciplineSettings : Settings
{

    [Setting]
    [DefaultValue(45)]
    [Category("General")]
    [DisplayName("Max Range")]
    [Description("Set to the max range (in yards) of your furthest-reaching damaging abillity.")]
    public float MaxRange { get; set; }
    [Setting]
    [DefaultValue(true)]
    [Category("General")]
    [DisplayName("Framelock")]
    [Description("Lock frames before each combat rotation (can help if it skips spells)")]
    public bool FrameLock { get; set; }
    
    [Setting]
    [DefaultValue(false)]
    [Category("General")]
    [DisplayName("Slow rotation for performance issues")]
    [Description("If you have performance issues with wRobot and the fightclasa, activate this. It will try to sleep until the next spell can be executed. This can and will cause some spells to skip.")]
    public bool SlowRotation { get; set; }


    public PriestDisciplineSettings()
    {
        FrameLock = true;
        SlowRotation = false;
        MaxRange = 45;
    }

    public static PriestDisciplineSettings CurrentSetting { get; set; }

    public bool Save()
    {
        try
        {
            return Save(AdviserFilePathAndName("CustomClass-ExampleSettings", ObjectManager.Me.Name + "." + Usefuls.RealmName));
        }
        catch (Exception e)
        {
            robotManager.Helpful.Logging.WriteError("ExampleSettings > Save(): " + e);
            return false;
        }
    }

    public static bool Load()
    {
        try
        {
            if (File.Exists(AdviserFilePathAndName("CustomClass-ExampleSettings", ObjectManager.Me.Name + "." + Usefuls.RealmName)))
            {
                CurrentSetting =
                    Load<PriestDisciplineSettings>(AdviserFilePathAndName("CustomClass-ExampleSettings",
                                                                 ObjectManager.Me.Name + "." + Usefuls.RealmName));
                return true;
            }
            CurrentSetting = new PriestDisciplineSettings();
        }
        catch (Exception e)
        {
            robotManager.Helpful.Logging.WriteError("ExampleSettings > Load(): " + e);
        }
        return false;
    }
}