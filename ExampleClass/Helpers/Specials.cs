using robotManager;
using wManager.Wow.Class;
using robotManager.Helpful;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using wManager;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using robotManager.FiniteStateMachine;
using wManager.Wow.Enums;

namespace Uhtred.Helpers
{
    public static class Tools
    {
        public static bool CheckLoS(Vector3 point1, Vector3 point2)
        {
            return !TraceLine.TraceLineGo(point1, point2, CGWorldFrameHitFlags.HitTestLOS);
        }
        public static bool CheckRange(Vector3 point1, Vector3 point2, float maxDist = 45, float minDist = 0)
        {
            float d = Helpers.Find.Distance(point1, point2);
            if (d > minDist && d < maxDist)
                return true;
            return false;
        }
    }
}

namespace Uhtred.Movement
{
    public static class Special
    {
        // I use this to repetetively push buttons for addon-interaction and whatnot
        // PLAN: impllement it as a "stop casting" feature to remove injection
        public static void PressKey(string key)
        {
            Keyboard.PressKey(wManager.Wow.Memory.WowMemory.Memory.WindowHandle, key);
        }

        public static void DemonHunterGlide()
        {
            wManager.Events.MovementEvents.OnMovementPulse += (path, c) =>
            {
                // saving our current position
                Vector3 myPosition = ObjectManager.Me.Position;

                
                var furthestPointInLoS = path.Where(p => !TraceLine.TraceLineGo(myPosition, p, wManager.Wow.Enums.CGWorldFrameHitFlags.HitTestAll)).OrderBy(p => ObjectManager.Me.Position.DistanceTo(p)).LastOrDefault();
                var distance = ObjectManager.Me.Position.DistanceTo(furthestPointInLoS);
                Spell FelRush = new wManager.Wow.Class.Spell("Fel Rush");
                Logging.WriteDebug($@"Beginning Demon Hunter 'Glide' ability with a jump");
                wManager.Wow.Helpers.Move.JumpOrAscend();
                Thread.Sleep(500);
                Logging.Write(@"Jump");
                wManager.Wow.Helpers.Move.JumpOrAscend();
                Logging.Write("NEED SOMETHING HERE");
                Thread.Sleep(500);
                Logging.Write(@"Jump");
                wManager.Wow.Helpers.Move.JumpOrAscend();
                FelRush.Launch();
                Logging.Write(@"Done");
                Logging.Write("MaxDistance= " + ObjectManager.Me.Position.DistanceTo(furthestPointInLoS) + ", Point = " + furthestPointInLoS.ToString());
            };
        }
    }

}
namespace Uhtred.Items
{
    public static class Special
    {
        public static void PutSafariHatOn()
        {
            ItemsManager.UseItem("Safari Hat");
        }
        public static int Count(string name)
        {
            return ItemsManager.GetItemCountByNameLUA(name);
        }
        public static bool HaveItem(string name)
        {
            return ItemsManager.GetItemCountByNameLUA(name) >= 1;
        }
        public static void Delete(string itemName)
        {
            // this is just lua code, same as you would use in a WoW macro
            string luaCode = $@"  for bag=0,4 do
                                        for slot=1, GetContainerNumSlots(bag) do
                                            local itemLink = GetContainerItemLink(bag, slot)
                                            if itemLink and string.find(itemLink, '{itemName}') then
                                                PickupContainerItem(bag, slot);
                                                DeleteCursorItem();
                                            end
                                        end
                                    end";
            // this is one of two ways to run lua code; the other ('Lua.LuaDoString') is 
            // is best for when you want it to return variables, this is faster for execution
            Lua.RunMacroText(luaCode);
        }
        public static bool Use(this string name)
        {
            if (!HaveItem(name))
                return false;
            ItemsManager.UseItem(name);
            return true;
        }

    }
}

    /*
    private void FightFix(WoWUnit unit, CancelEventArgs e)
    {
        // Not in LoS
        if (TraceLine.TraceLineGo(unit.Position))
        {
            // Get waypoints to the target
            Log("Not in Line of Sight.");
            var waypoints = PathFinder.FindPath(unit.Position, true);
            foreach (Vector3 waypoint in waypoints)
            {
                if (!TraceLine.TraceLineGo(waypoint))
                {
                    // In LoS
                    MovementManager.MoveTo(waypoint);
                    // While moving....
                }
                break;
            }
        }
        var f = Fight.StartFight(unit.Guid);
    }
    private void LootingEventHandler(WoWUnit unit)
    {
        foreach (string itemName in ItemsToDestroy)
        {
            if (HaveItemNamed(itemName))
            {
                DeleteItemNamed(itemName);
                Thread.Sleep(500);
            }
        }
        foreach (string itemName in ItemsToUse)
        {
            if (HaveItemNamed(itemName))
            {
                UseItemNamed(itemName);
                Thread.Sleep(500);
            }
        }
    }
    public void LuaEventHandler(string id, List<string> args)
    {
        if (id == "PET_BATTLE_OPENING_DONE")
        {
            string logMessage = @"Pressing ""A"" because the lua event ""PET_BATTLE_OPENING_DONE"" has fired";
            Logging.WriteDebug(logMessage);
            tdBattlePetScriptAuto();           //DoPetBattle() ;
        }
    }
    public static void OnRadar3DDrawEvent()
    {
        wManager.Wow.Helpers.Radar3D.DrawString("This is sample text", ObjectManager.Me.Position, 14f, System.Drawing.Color.Green, 255, System.Drawing.FontFamily.GenericSansSerif);
    }
    public void TripleJump(List<Vector3> points, CancelEventArgs cancelable)
    {
        switch (points)
        {
            case List<Vector3> multipleVectors:
                if (ObjectManager.Me.WowClass == wManager.Wow.Enums.WoWClass.DemonHunter && !ObjectManager.Me.IsFlying && !ObjectManager.Me.IsMounted)
                {
                    Vector3 positionMe = ObjectManager.Me.Position;
                    var furthestPointInLoS = points.Where(p => !TraceLine.TraceLineGo(positionMe, p, wManager.Wow.Enums.CGWorldFrameHitFlags.HitTestAll)).OrderBy(p => ObjectManager.Me.Position.DistanceTo(p)).LastOrDefault();
                    //wManager.Wow.Helpers.Move.JumpOrAscend;
                    MovementManager.MoveTo(furthestPointInLoS);
                }
        }

        if (InPetBattle)
        {
            cancelable.Cancel = true;
        }
        if (ObjectManager.Me.IsFallingFar || ObjectManager.Me.WowClass == wManager.Wow.Enums.WoWClass.DemonHunter && ObjectManager.Me.Position.IsGround() && !ObjectManager.Me.IsMounted && robotManager.Products.Products.ProductName != "Gatherer")
        {
            TripleJump();
        }
    }
    public void MovementHandler(Vector3 point, CancelEventArgs cancelable)
    {
        MovementHandler(new List<Vector3> { point }, cancelable);
    }

    public void Settings()
    {
        Watcher.Load();
        Watcher.CurrentSettings.ToForm();
        Watcher.CurrentSettings.Save();
        Log("Settings saved.");
    }

    public static bool InPetBattle { get { return Lua.LuaDoString<bool>("return C_PetBattles.IsInBattle()"); } }

    public bool HaveHurtPets
    {
        get
        {
            
            var pets = PetBattles.PetJournalGetExistPetActiveId();
            foreach (int petId in pets)
            {
                int MaxHealth = PetBattles.GetMaxHealth(PetBattles.PetFaction.Ally, petId);
                int Health = PetBattles.GetHealth(PetBattles.PetFaction.Ally, petId);
                if (Health < MaxHealth)
                    return true;
            }
            return false;
        }
    }
    public bool CanFly
    {
        get { return Lua.LuaDoString<bool>("return IsFlyableArea()"); }
    }
    public List<WoWUnit> GetTargetsByName(List<string> TargetList)
    {
        return ObjectManager.GetWoWUnitByName(TargetList).Where(u => u.IsValid && u.IsAlive).OrderBy(o => ObjectManager.Me.Position.DistanceTo(o.Position)).ToList();
    }
    public void Kill(IEnumerable<WoWUnit> KillList)
    {
        WoWUnit target = KillList.OrderBy(o => ObjectManager.Me.Position.DistanceTo(o.Position)).ToList().First();
        var path = PathFinder.FindPath(target.Position, true);
        MovementManager.Go(path);
        if (target.GetDistance < 35)
        {
            var f = Fight.StartFight(target.Guid, false);
        }
    }
    public void DoPetBattle()
    {
        while (InPetBattle)
        {
            tdBattlePetScriptAuto();
            Thread.Sleep(500);
        }
    }
    public void PluginLoop()
    {
        while (Launched)
        {
            if (InPetBattle)
            {
                DoPetBattle();
            }
            if (NearbyTargetCount > 0)
            {
                Log("Killing target.");
                Kill(NearbyTargets);
            }
            if ((NearbyRareCount > 0) && (NearbyRareCount == 0) && KillAllRares)
            {
                Log("Killing rare.");
                Kill(NearbyRares);
            }
            if (NearbyCrittertCount > 0 && KillCritters)
            {
                Log("Killing critters...");
                Kill(CrittersToKill);
            }
            Thread.Sleep(1000);
        }
    }
}

public class Watcher : Settings
{
    public Watcher()
    {
        KillAllRaresSetting = false;
        KillAllCrittersSetting = false;
        PrioritizeWatchlistSetting = true;
        string[] WatchlistArray = new string[] { "", };
        string[] ItemsToDestroyArray = new string[] { "", };
        string[] ItemsToUseArray = new string[] { "", };
    }
    public static Watcher CurrentSettings { get; set; }
    public bool Save()
    {
        try
        {
            return Save(AdviserFilePathAndName("Watcher", ObjectManager.Me.Name + "." + Usefuls.RealmName));
        }
        catch (Exception e)
        {
            Logging.WriteError("Watcher > Save(): " + e);
            return false;
        }
    }
    public static bool Load()
    {
        try
        {
            if (File.Exists(AdviserFilePathAndName("Watcher", ObjectManager.Me.Name + "." + Usefuls.RealmName)))
            {
                CurrentSettings = Load<Watcher>(AdviserFilePathAndName("Watcher", ObjectManager.Me.Name + "." + Usefuls.RealmName));
                return true;
            }
            CurrentSettings = new Watcher();
        }
        catch (Exception e)
        {
            Logging.WriteError("Watcher > Load(): " + e);
        }
        return false;
    }
    [Setting]
    [Category("Kill Settings")]
    [DisplayName("Kill All Rare Elites?")]
    [Description("T/F?")]
    public bool KillAllRaresSetting { get; set; }

    [Setting]
    [Category("Kill Settings")]
    [DisplayName("Kill Critters?")]
    [Description("T/F?")]
    public bool KillAllCrittersSetting { get; set; }

    [Setting]
    [Category("Kill Settings")]
    [DisplayName("Prioritize Watch List")]
    [Description(@"If both options are selected, should the ""Watch List"" take priority over the rare elites?")]
    public bool PrioritizeWatchlistSetting { get; set; }

    [Setting]
    [Category("Kill Settings")]
    [DisplayName("Watch List")]
    [Description("Which units should we be watching for?")]
    public string[] WatchlistArray { get; set; }

    [Setting]
    [Category("Loot Settings")]
    [DisplayName("Items to destroy")]
    [Description("List of items to destroy if they are looted (or in your bags)")]
    public string[] ItemsToDestroyArray { get; set; }

    [Setting]
    [Category("Loot Settings")]
    [DisplayName("Items to use")]
    [Description("List of items to use if they are looted (or in your bags)")]
    public string[] ItemsToUseArray { get; set; }
}

namespace WowUI
{
    public class Frames
    {
        string createFrameString = @"local frame = CreateFrame(""{0}"", ""{1}"", {2}, ""{3}"", {4})" ; //	frameRef, frameType, frameName, parentFrame, inheritsFrame, id
        static void CreateFrame(string frameRef, string frameType, string frameName, string parentFrame, string inheritsFrame, int id)
        {
            Lua.LuaDoString(createFrameString.Format(frameRef, frameType, frameName, parentFrame, inheritsFrame, id));
        }
    }
}
*/