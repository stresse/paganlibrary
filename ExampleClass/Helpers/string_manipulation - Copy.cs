﻿using robotManager;
using wManager.Wow.Class;
using robotManager.Helpful;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using wManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using robotManager.FiniteStateMachine;

// PLUGIN FUNCTIONALITY
namespace Uhtred.Helpers
{
    partial class FormatTo
    {
        public static string Lua(string input)
        {
            return input.Replace("'", "\\'").Replace("\"", "\\\"");
        }

        public static string String(params object[] inputs)
        {
            int errorCount = 0;
            string logString = "";
            foreach (object input in inputs)
            {
                try
                {
                    logString += input.ToString();
                }
                catch (Exception e)
                {
                    logString += "[-ERROR-" + e + "-ERROR-]"; errorCount ++;
                }
            }
            if (errorCount > 0)
            {
                logString += $"\n(Error Count : {errorCount})";
            }
            return logString;
        }
        /*
        public static void Log(string header, string message)
        {
            string logHeader = $"[{header}]";
            string logString = $"{logHeader} {message}";
            string logStringLua = $"DEFAULT_CHAT_FRAME:AddMessage(\"|CFF008000{logHeader}|r :{message}\")";
            Logging.Write(logString);
            wManager.Wow.Helpers.Lua.RunMacroText(logStringLua);
        }
        public static void Log(string message)
        {
            string productName = robotManager.Products.Products.ProductName;
            Log($"[productName] [stresse 'Log' function]", message);
        }
    }


}