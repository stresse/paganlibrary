using System.Collections.Generic;
using System.Linq;
using System.Threading;
using robotManager.Helpful;
using wManager;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;

namespace CombatRotation.RotationFramework
{
	public class RotationFramework
	{
		private static RotationSpellbook _rotationSpellbook;
		private static bool _slowRotation = false;
		private static bool _framelock = true;

		private static WoWLocalPlayer player = ObjectManager.Me;
		private static WoWUnit pet = ObjectManager.Pet;
		private static WoWUnit target = ObjectManager.Target;
		private static List<WoWUnit> units = ObjectManager.GetObjectWoWUnit();

		internal static bool Casting { get; set; }

		public static void Initialize(bool slowRotation = false, bool framelock = true)
		{
			if (wManager.Wow.Memory.WowMemory.FrameIsLocked)
				wManager.Wow.Memory.WowMemory.UnlockFrame();

			_rotationSpellbook = new RotationSpellbook();
			_slowRotation = slowRotation;
			_framelock = framelock;
			RotationEventHandler.Start();
		}

		public static void Dispose()
		{
			if (wManager.Wow.Memory.WowMemory.FrameIsLocked)
				wManager.Wow.Memory.WowMemory.UnlockFrame();

			RotationEventHandler.Stop();
		}

		public static void RunRotation(List<RotationStep> rotation)
		{
			float globalCd = GetGlobalCooldown();
			bool gcdEnabled = globalCd != 0;
			Casting = Me.IsCast || Me.IsCasting();

			if (_slowRotation)
			{
				if (Casting)
				{
					var temp = Me.CastingTimeLeft;
					RotationLogger.Fight($"Slow rotation - still casting! Wait for {temp + 100}");
					Thread.Sleep(temp + 100);
				}
				//if no spell was executed successfully, we are assuming to still be on GCD and sleep the thread until the GCD ends
				//this prevents the rotation from re-checking if a no-gcd spell like Vanish, Judgement etc is ready
				else if (gcdEnabled)
				{
					RotationLogger.Fight($"No spell cast, but waiting {(globalCd + 100)}ms for global cooldown to end!");
					Thread.Sleep((int) (globalCd + 100));
				}
			}


			var watch = System.Diagnostics.Stopwatch.StartNew();

			if (_framelock)
				RunInFrameLock(rotation, gcdEnabled);
			else
				RunInLock(rotation, gcdEnabled);
			watch.Stop();
			if (watch.ElapsedMilliseconds > 150)
				RotationLogger.Fight($"Iteration took {watch.ElapsedMilliseconds}ms");
		}

		private static void RunInLock(List<RotationStep> rotation, bool gcdEnabled)
		{
			lock (ObjectManager.Locker)
			{
				UpdateUnits();
				foreach (var step in rotation)
					if (step.ExecuteStep(gcdEnabled))
						break;
			}
		}

		private static void RunInFrameLock(List<RotationStep> rotation, bool gcdEnabled)
		{
			if (_framelock)
				wManager.Wow.Memory.WowMemory.LockFrame();

			UpdateUnits();

			foreach (var step in rotation)
				if (step.ExecuteStep(gcdEnabled))
					break;

			if (_framelock)
				wManager.Wow.Memory.WowMemory.UnlockFrame();
		}

		private static void UpdateUnits()
		{
			player = ObjectManager.Me;
			target = ObjectManager.Target;
			pet = ObjectManager.Pet;
			List<WoWUnit> relevantUnits = new List<WoWUnit>();
			relevantUnits.AddRange(ObjectManager.GetObjectWoWPlayer().Where(u => u.GetDistance <= 50));
			relevantUnits.AddRange(ObjectManager.GetObjectWoWUnit().Where(u => u.GetDistance <= 50));
			units.Clear();
			units.AddRange(relevantUnits);
		}

		public static WoWLocalPlayer Me => player;
		public static WoWUnit Target => target;
		public static WoWUnit Pet => pet;
		public static List<WoWUnit> Units => units;

		public static float GetGlobalCooldown()
		{
			return SpellManager.GlobalCooldownTimeLeft() * 1000f;
		}

		public static float GetItemCooldown(string itemName)
		{
			string spellName = ItemsManager.GetItemSpell(itemName);
			float cooldown = SpellManager.GetSpellCooldownTimeLeft(spellName) * 1000f;
			return cooldown;
		}
	}
}